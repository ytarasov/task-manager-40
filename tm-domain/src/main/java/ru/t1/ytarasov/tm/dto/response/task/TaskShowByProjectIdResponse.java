package ru.t1.ytarasov.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.ytarasov.tm.dto.response.AbstractResponse;
import ru.t1.ytarasov.tm.model.Task;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public final class TaskShowByProjectIdResponse extends AbstractResponse {

    @NotNull
    private List<Task> tasks;

    public TaskShowByProjectIdResponse(@NotNull List<Task> tasks) {
        this.tasks = tasks;
    }

}
