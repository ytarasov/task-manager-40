package ru.t1.ytarasov.tm.api.service;

import org.apache.ibatis.annotations.Param;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.enumerated.Sort;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    @NotNull
    Task add(@Nullable final Task task) throws Exception;

    @NotNull
    Task create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

    @Nullable
    List<Task> findAll();

    @Nullable
    List<Task> findAll(@Nullable final String userId) throws Exception;

    @Nullable
    List<Task> findAll(@Nullable final Comparator comparator) throws Exception;

    @Nullable
    List<Task> findAll(
            @Nullable final String userId,
            @Nullable final Comparator comparator
    ) throws Exception;

    @Nullable
    List<Task> findAll(@Nullable final Sort sort) throws Exception;

    @Nullable
    List<Task> findAll(@Nullable final String userId, @Nullable final Sort sort) throws Exception;

    @Nullable
    Task findOneById(@Nullable final String id) throws Exception;

    @Nullable
    Task findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws Exception;

    int getSize() throws Exception;

    int getSize(@Nullable final String userId) throws Exception;

    @NotNull
    Task remove(@Nullable final Task task) throws Exception;

    @Nullable
    Task removeById(@Nullable final String id) throws Exception;

    @Nullable
    Task removeById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws Exception;

    boolean existsById(@Nullable final String id) throws Exception;

    boolean existsById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws Exception;

    @NotNull
    List<Task> findAllTasksByProjectId(
            @Nullable String userId,
            @Nullable String projectId
    ) throws Exception;

    @NotNull
    Task update(@NotNull Task task) throws Exception;

    @NotNull
    Task updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

    @NotNull
    Task changeTaskStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    ) throws Exception;

    void clear() throws Exception;

    void clear(@Nullable final String userId) throws Exception;

}
