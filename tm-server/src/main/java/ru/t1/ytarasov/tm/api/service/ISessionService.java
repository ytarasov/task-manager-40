package ru.t1.ytarasov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.model.Session;

public interface ISessionService {

    @NotNull
    Session add(@Nullable Session session) throws Exception;

    @Nullable
    Session findOneById(@Nullable final String id) throws Exception;

    boolean existsById(@Nullable final String id) throws Exception;

    @NotNull
    Session remove(@Nullable final Session session) throws Exception;
}
